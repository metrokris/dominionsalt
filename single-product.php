<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<?php
    // get the post
    global $post, $product;

?>
<?php while ( have_posts() ) : the_post(); ?>
    <?php start_short(); ?>

    <!-- banner -->
    <?php
        // get the product category
        $terms = get_the_terms( $post->ID, 'product_cat' );
        foreach ($terms as $term) {
            $product_slug = $term->slug;
            break;
        }

        // the banner field
        $banner_field = 'pacific_banner ';

        // select the corresponding banner
        switch($product_slug){
            case 'pharmaceutical':
                $banner_field = 'industry_pharmaceutical_banner';             
            break;
            case 'flaky-sea-salt-natural':
                $banner_field = 'industry_flaky_banner';
            break;
            case 'animal-nutrition-agriculture':
                $banner_field = 'industry_agriculture_banner';             
            break;
            case 'food':
                $banner_field = 'industry_food_banner';             
            break;
            case 'trade-industrial':
                $banner_field = 'industry_trade_banner';             
            break;
            case 'wt-pools':
                $banner_field = 'industry_pool_banner';             
            break;
        }

        // the generic header id
        $generic_id = 62;
        
        // get the banner field
        $banner_url = get_post_meta($generic_id, $banner_field); 
        
        // get the attachment 
        $attachment = wp_get_attachment_image_src( $banner_url[0], 'full' );
        
        // get the banner image
        $banner = $attachment[0];
       
    ?>
    [et_pb_section admin_label="Section" fullwidth="on" specialty="off"]
        [et_pb_fullwidth_header admin_label="Fullwidth Header" background_layout="dark" text_orientation="center" header_fullscreen="off" header_scroll_down="off" background_url="<?php echo $banner; ?>" parallax="off" parallax_method="off" content_orientation="center" image_orientation="center" custom_button_one="off" button_one_letter_spacing="0" button_one_use_icon="default" button_one_icon_placement="right" button_one_on_hover="on" button_one_letter_spacing_hover="0" custom_button_two="off" button_two_letter_spacing="0" button_two_use_icon="default" button_two_icon_placement="right" button_two_on_hover="on" button_two_letter_spacing_hover="0"]
        [/et_pb_fullwidth_header]
    [/et_pb_section]
    <!-- /banner -->

    <!-- sub banner -->
    [et_pb_section admin_label="Section" global_module="1450" fullwidth="on" specialty="off" transparent_background="off" background_color="#ffffff" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"]
        [et_pb_fullwidth_image global_parent="1450" admin_label="Fullwidth Image" src="http://dominionsalt.kinsta.com/wp-content/uploads/2017/03/header-background.jpg" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="off" use_border_color="off" border_color="#ffffff" border_style="solid"]
        [/et_pb_fullwidth_image]
    [/et_pb_section]
    <!-- /sub banner -->


    <!-- product content -->
    [et_pb_section admin_label="Section" fullwidth="off" specialty="off"]
        [et_pb_row admin_label="Row"]
            [et_pb_column type="4_4"]
                [et_pb_text admin_label="Text" background_layout="light" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]
                    <h1><?php the_title(); ?></h1>
                [/et_pb_text]
            [/et_pb_column]
        [/et_pb_row]
        [et_pb_row admin_label="Row"]
            [et_pb_column type="1_3"]
                [et_pb_image admin_label="Image" src="<?php the_post_thumbnail_url('full'); ?>" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="off" sticky="off" align="left" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid"] 
                [/et_pb_image]
            [/et_pb_column]
            [et_pb_column type="2_3"]
                [et_pb_text admin_label="Text" background_layout="light" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]
                    <?php the_content(); ?>
                [/et_pb_text]
                [et_pb_code admin_label="Code"]
                    <?php 
                        // get the product meta
                        $meta = get_post_meta( get_the_id() );

                        // specification file
                        $meta_specification = $meta['specification_file'][0];
                        $file_specification = get_the_guid( $meta_specification ); 
                        
                        // resource file
                        $meta_resource =  $meta['resource_file'][0];
                        $file_resource = get_the_guid( $meta_resource );
                        
                    ?>
                    <div class='download-files'>
                        <?php if( $meta_specification ){ ?>
                        <span>
                            <a href="<?php echo $file_specification; ?>" target="_blank">
                                <img src="/wp-content/uploads/2017/03/Medium-Blue.png" />
                            </a>
                            <br />
                            Specification file
                        </span>
                        <?php } ?>

                        <?php if( $meta_resource ){ ?>
                        <span>
                            <a href="<?php echo $file_resource; ?>" target="_blank">'
                                <img src="/wp-content/uploads/2017/03/Medium-Blue.png" />
                            </a>
                            <br />
                            Resource file
                        </span>
                        <?php } ?>
                    </div>
                [/et_pb_code]
            [/et_pb_column]
        [/et_pb_row]
    [/et_pb_section]
    <!-- /product content -->

    <!-- gallery -->
    <?php 
        //  get the gallery image ids
        $attachment_ids = $product->get_gallery_attachment_ids();
       
        // convert arrays into csv
        $gallery_ids = implode(",", $attachment_ids);

    ?>
    <?php 
        if( !empty($attachment_ids) ):
    ?>
    [et_pb_section admin_label="Section" fullwidth="off" specialty="off" transparent_background="off" background_color="#efefef" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"]
        [et_pb_row admin_label="Row" make_fullwidth="off" use_custom_width="off" width_unit="on" use_custom_gutter="on" padding_mobile="off" background_color="#efefef" allow_player_pause="off" parallax="off" parallax_method="off" make_equal="off" parallax_1="off" parallax_method_1="off" column_padding_mobile="on" gutter_width="1"]
            [et_pb_column type="4_4"]
                [et_pb_text admin_label="Text" background_layout="light" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]
                    <h2>Gallery</h2>
                    &nbsp;
                [/et_pb_text]
                [et_pb_gallery admin_label="Gallery" gallery_ids=<?php echo $gallery_ids; ?> fullwidth="off" show_title_and_caption="off" show_pagination="off" background_layout="light" auto="off" hover_overlay_color="rgba(255,255,255,0.9)" caption_all_caps="off" use_border_color="off" border_color="#ffffff" border_style="solid" posts_number="8"] 
                [/et_pb_gallery]
            [/et_pb_column]
        [/et_pb_row]
    [/et_pb_section]
    <?php endif; ?>
    <!-- /gallery -->


    <!-- downloadable files -->
    <?php 
        $downloads = $product->get_files();
        
        // if the array is empty
        if( !empty($downloads) ):
        
        foreach( $downloads as $download):
    ?>
    [et_pb_section admin_label="Section" fullwidth="off" specialty="off" transparent_background="off" background_color="#dc1d49" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" custom_padding="0px|0px|0px|0px" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"]
        [et_pb_row admin_label="Row" make_fullwidth="on" use_custom_width="off" width_unit="on" use_custom_gutter="on" gutter_width="1" custom_padding="0px|0px|0px|0px" padding_mobile="on" allow_player_pause="off" parallax="off" parallax_method="off" make_equal="off" parallax_1="off" parallax_method_1="off" column_padding_mobile="on" padding_top_1="2.5%" padding_right_1="2.5%" padding_bottom_1="2.5%" padding_left_1="2.5%" module_class="fullwidth-cta" padding_1_tablet="5%|5%|5%|5%" padding_1_last_edited="on|phone"]
            [et_pb_column type="4_4"]
                [et_pb_text admin_label="Text" background_layout="dark" text_orientation="center" use_border_color="off" border_color="#ffffff" border_style="solid" text_font_size="24"]
                    Download <?php echo $download['name']; ?> <span class="arrow">→</span>
                [/et_pb_text]
                [et_pb_button admin_label="Button" button_url="<?php echo $download['file']; ?>" url_new_window="on" button_text="Contact Yates Design" button_alignment="center" background_layout="dark" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0" custom_css_main_element="zoom: 1;||filter: alpha(opacity=0);||opacity: 0;||position: absolute;||top: 0;||left: 0;||width: 100%;||height: 100%;"] 
                [/et_pb_button]
                [et_pb_code admin_label="Code - CSS"]
                    &lt;style&gt; .fullwidth-cta, .fullwidth-cta .arrow { -webkit-transition: all 0.2s; -moz-transition: all 0.2s; transition: all 0.2s; } .fullwidth-cta:hover { background: #4CD964; } .fullwidth-cta:hover .arrow { padding-left: 5px; } &lt;/style&gt;
                [/et_pb_code]
            [/et_pb_column]
        [/et_pb_row]
    [/et_pb_section]
    <?php
        endforeach;
        endif; 
    ?>
    <!-- /downloadble files -->

        


 
<?php end_short(); ?>
<?php endwhile; // end of the loop. ?>

<?php get_footer( 'shop' ); ?>