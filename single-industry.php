<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php start_short(); ?>

		<!-- banner -->	
		[et_pb_section admin_label="Section" fullwidth="on" specialty="off"][et_pb_fullwidth_header admin_label="Fullwidth Header" title="<?php do_short('[field title]'); ?>" background_layout="dark" text_orientation="center" header_fullscreen="off" header_scroll_down="off" background_url="<?php do_short('[acf field=banner]'); ?>" parallax="off" parallax_method="off" content_orientation="center" image_orientation="center" custom_button_one="off" button_one_letter_spacing="0" button_one_use_icon="default" button_one_icon_placement="right" button_one_on_hover="on" button_one_letter_spacing_hover="0" custom_button_two="off" button_two_letter_spacing="0" button_two_use_icon="default" button_two_icon_placement="right" button_two_on_hover="on" button_two_letter_spacing_hover="0"]
			[/et_pb_fullwidth_header]
		[/et_pb_section]
		<!-- /banner -->

		<!-- sub banner -->
		[et_pb_section admin_label="Section" global_module="1450" fullwidth="on" specialty="off" transparent_background="off" background_color="#ffffff" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"]
			[et_pb_fullwidth_image global_parent="1450" admin_label="Fullwidth Image" src="http://dominionsalt.kinsta.com/wp-content/uploads/2017/03/header-background.jpg" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="off" use_border_color="off" border_color="#ffffff" border_style="solid"]
			[/et_pb_fullwidth_image]
		[/et_pb_section]
		<!-- /sub banner -->

		<!-- content -->
		[et_pb_section admin_label="Section" fullwidth="off" specialty="off"]
			[et_pb_row admin_label="Row"]
				[et_pb_column type="4_4"]
					[et_pb_text admin_label="Text" background_layout="light" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]
						[content]
					[/et_pb_text]
				[/et_pb_column]
			[/et_pb_row]
		[/et_pb_section]
		<!-- /content -->

		<!-- load all subcategories -->
		<!-- subcategories -->
		[et_pb_section admin_label="section"]
			[et_pb_row admin_label="row"]
				[et_pb_column type="4_4"]
					[et_pb_code admin_label="Code"]
						<div class="cards card1">
						<?php 
						  	// get the product categpory id
						  	$parent_cat = get_field('product_category');

						  	// get the product sub categories
						  	$terms = get_terms( 'product_cat', array(
						        'hide_empty' => 0,
						        'parent' => $parent_cat,
						  	));

						  	// loop thru the records
					        foreach( $terms as $term):
					
								// get category thumbnail id
								$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
								
								// get the thumbnail image
								$image = wp_get_attachment_url( $thumbnail_id, 'full_size' );
						?>
							
								<figure class="item">
									<div class='image' style='background-image:url(<?php echo $image; ?>);'></div>
									<figcaption>
										<h4><?php echo $term->name; ?></h4>
									</figcaption>
									<a href="<?php echo get_term_link( $term->term_id, 'product_cat'); ?>"></a>
								</figure>	
						<?php	
							endforeach;
					  	?>
					  	</div>
					[/et_pb_code]
				[/et_pb_column]
			[/et_pb_row]
		[/et_pb_section]
		<!-- /subcategories -->
	

		<?php end_short(); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>