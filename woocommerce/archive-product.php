<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly

}
	global $post;

	// get the post id
    $post_id=$post->ID;

get_header( 'shop' ); 

?>

	<?php start_short(); ?>

		<!-- banner -->	
		<?php
			// get the category by post id
			$post_cat = get_the_terms($post_id, 'product_cat');

			// get the parent category
 			$product_slug = $post_cat[0]->slug;
 		
 			// select the corresponding banner
	        switch($product_slug){
	            case 'pharmaceutical':
	                $banner_field = 'industry_pharmaceutical_banner';             
	            break;
	            case 'flaky-sea-salt-natural':
	                $banner_field = 'industry_flaky_banner';
	            break;
	            case 'animal-nutrition-agriculture':
	                $banner_field = 'industry_agriculture_banner';             
	            break;
	            case 'food':
	                $banner_field = 'industry_food_banner';             
	            break;
	            case 'trade-industrial':
	                $banner_field = 'industry_trade_banner';             
	            break;
	            case 'wt-pools':
	                $banner_field = 'industry_pool_banner';             
	            break;
	            default:
	            	$banner_field = 'product_category_banner';
	            break;
	        }
	        
			 // the generic header id
	        $generic_id = 62;
	        
	        // get the banner field
	        $banner_url = get_post_meta($generic_id, $banner_field); 
	        
	        // get the attachment 
	        $attachment = wp_get_attachment_image_src( $banner_url[0], 'full' );
	        
	        // get the banner image
	        $banner = $attachment[0];

			
		?>


		[et_pb_section admin_label="Section" fullwidth="on" specialty="off"][et_pb_fullwidth_header admin_label="Fullwidth Header" title="<?php woocommerce_page_title(); ?>" background_layout="dark" text_orientation="center" header_fullscreen="off" header_scroll_down="off" background_url="<?php   echo $banner; ?>" parallax="off" parallax_method="off" content_orientation="center" image_orientation="center" custom_button_one="off" button_one_letter_spacing="0" button_one_use_icon="default" button_one_icon_placement="right" button_one_on_hover="on" button_one_letter_spacing_hover="0" custom_button_two="off" button_two_letter_spacing="0" button_two_use_icon="default" button_two_icon_placement="right" button_two_on_hover="on" button_two_letter_spacing_hover="0"]
			[/et_pb_fullwidth_header]
		[/et_pb_section]
		<!-- /banner -->

		<!-- sub banner -->
		[et_pb_section admin_label="Section" global_module="1450" fullwidth="on" specialty="off" transparent_background="off" background_color="#ffffff" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"]
			[et_pb_fullwidth_image global_parent="1450" admin_label="Fullwidth Image" src="http://dominionsalt.kinsta.com/wp-content/uploads/2017/03/header-background.jpg" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="off" use_border_color="off" border_color="#ffffff" border_style="solid"]
			[/et_pb_fullwidth_image]
		[/et_pb_section]
		<!-- /sub banner -->

		<!-- content -->
		[et_pb_section admin_label="Section" fullwidth="off" specialty="off"]
			[et_pb_row admin_label="Row"]
				[et_pb_column type="4_4"]
					[et_pb_text admin_label="Text" background_layout="light" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]
						<?php 
							// get category description by category slug
							echo category_description(); 
						?>
					[/et_pb_text]
				[/et_pb_column]
			[/et_pb_row]
		[/et_pb_section]
		<!-- /content -->
		
		<!-- load products -->	
		[et_pb_section admin_label="section"]
			[et_pb_row admin_label="Row"]
				[et_pb_column type="1_3"]					
					<?php dynamic_sidebar(1); ?>
				[/et_pb_column]
				[et_pb_column type="2_3"]
					[et_pb_code admin_label="Code"]
						<?php
							/**
							 * woocommerce_before_shop_loop hook.
							 *
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							//do_action( 'woocommerce_before_shop_loop' );
						?>

						<?php woocommerce_product_loop_start(); ?>

							<?php woocommerce_product_subcategories(); ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php wc_get_template_part( 'content', 'product' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php woocommerce_product_loop_end(); ?>

						<?php
							/**
							 * woocommerce_after_shop_loop hook.
							 *
							 * @hooked woocommerce_pagination - 10
							 */
							//do_action( 'woocommerce_after_shop_loop' );
						?>
					[/et_pb_code]
				[/et_pb_column]
			[/et_pb_row]
		[/et_pb_section]
		<!-- /load products -->
	
	<?php end_short(); ?>


<?php get_footer( 'shop' ); ?>
