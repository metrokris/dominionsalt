(function($){

  $(document).ready(function(){

    (function(){

      var colors = new Array(
        [0,180,241],
        [0,79,163],
        [255, 242, 0],
        [0,180,241],
        [0,79,163],
        [255, 242, 0],
        [0,180,241],
        [0,79,163]);

      var step = 0;
      //color table indices for: 
      // current color left
      // next color left
      // current color right
      // next color right
      var colorIndices = [0,1,2,3];

      //transition speed
      var gradientSpeed = 0.002;

      function updateGradient()
      {
        
        if ( $===undefined ) return;
        
      var c0_0 = colors[colorIndices[0]];
      var c0_1 = colors[colorIndices[1]];
      var c1_0 = colors[colorIndices[2]];
      var c1_1 = colors[colorIndices[3]];

      var istep = 1 - step;
      var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
      var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
      var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
      var color1 = "rgb("+r1+","+g1+","+b1+")";

      var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
      var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
      var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
      var color2 = "rgb("+r2+","+g2+","+b2+")";

       $('#top-header').css({
         background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
          background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});
        
        step += gradientSpeed;
        if ( step >= 1 )
        {
          step %= 1;
          colorIndices[0] = colorIndices[1];
          colorIndices[2] = colorIndices[3];
          
          //pick two new target color indices
          //do not pick the same as the current one
          colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
          colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
          
        }
      }

      setInterval(updateGradient,10);

    })();
  

  (function() {
        var showcase = $("#showcase");

        showcase.Cloud9Carousel( {
          yPos: 42,
          yRadius: 48,
          mirrorOptions: {
            gap: 12,
            height: 0.2
          },
          buttonLeft: $(".nav > .left"),
          buttonRight: $(".nav > .right"),
          autoPlay: true,
          bringToFront: true,
          onRendered: showcaseUpdated,
          onLoaded: function() {
            showcase.css( 'visibility', 'visible' )
            showcase.css( 'display', 'none' )
            showcase.fadeIn( 1500 )
          }
        });


        function showcaseUpdated( showcase ) {

          // get the alt tag 
          var description = showcase.nearestItem().element.alt;
          //console.log(description);


          // display text module tag
          var title =  $('.cloud9-text.'+description).show(); //html(description);

          var c = Math.cos((showcase.floatIndex() % 1) * 2 * Math.PI);
          title.css('opacity', 0.5 + (0.5 * c));
        }

        // Simulate physical button click effect
        $('.nav > button').click( function( e ) {
          var b = $(e.target).addClass( 'down' )
          setTimeout( function() { b.removeClass( 'down' ) }, 80 )
        } )

        $(document).keydown( function( e ) {
          //
          // More codes: http://www.javascripter.net/faq/keycodes.htm
          //
          switch( e.keyCode ) {
            /* left arrow */
            case 37:
              $('.nav > .left').click()
              break

            /* right arrow */
            case 39:
              $('.nav > .right').click()
          }
        } )
      })();


  });  // document ready
})(jQuery);
