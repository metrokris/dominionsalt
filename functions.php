<?php

//import parent theme
add_action( 'wp_enqueue_scripts', 'my_child_theme_scripts' );
function my_child_theme_scripts() {
    wp_enqueue_style( 'parent-theme-css', get_template_directory_uri() . '/style.css' );

    // import js files
    wp_enqueue_script( 'jquery-cloud9carousel', get_template_directory_uri() . '/../divi-child/js/jquery.cloud9carousel.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'jquery-reflection', get_template_directory_uri() . '/../divi-child/js/jquery.reflection.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'app', get_template_directory_uri() . '/../divi-child/js/app.js', array ( 'jquery' ), 1.1, true);
}





//make gravity forms available editors
function add_grav_forms(){
    $role = get_role('editor');
    $role->add_cap('gform_full_access');
}
add_action('admin_init','add_grav_forms');






/**
 * Remove Divi Builder "project" type
 */
if ( ! function_exists( 'et_pb_register_posttypes' ) ) :
function et_pb_register_posttypes() {
	global $wp_post_types;
	if ( isset( $wp_post_types[ $post_type ] ) ) {
		unset( $wp_post_types[ $post_type ] );
		return true;
	}
	return false;
}
endif;


/**
 * Hide Divi Builder "project" type
 */
add_filter('et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1);
function mytheme_et_project_posttype_args($args) {
  return array_merge($args, array(
    'public' => false
  ));
}


// this shortcode will return the specification files
function get_product_files( $atts ){
	
	// get all attached resources
	$args = array( 
				'post_type' => 'product',							
			);
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		
		$html = '<ul class="pdf-list">';
		while ( $the_query->have_posts() ) {
			
			// set the post
			$the_query->the_post();

			// get the product meta data by passing the id
			$product = get_post_meta( get_the_id() );

			// identify which file field to load
			$file_field = $atts['file'] . '_file';

			// get the attachment id
			$attachment_id = $product[$file_field][0];
			
			// check if their is a file attached
			$response;
			if($attachment_id){
				
				$html = $html . '
							<li>
								<div class="attachment-container">
									<a href="'.get_the_guid($attachment_id).'" target="_blank"><img src="/wp-content/uploads/2017/03/Medium-Blue.png" /></a>
									<div>' .get_the_title(). '</div>
								</div>
							</li>
						';
			}
		}
		$html = $html . '<ul>';

		/* Restore original Post Data */
		wp_reset_postdata();
	} else {
		// no posts found
		$response = 'no specification files';
	}
			
	return $html;
}
add_shortcode( 'get-product-files', 'get_product_files' ); 



// this shortcode will return the product parent categories with images
function get_parent_categories(){
	// store of the results
	$response;

	// get the parent categories
  	$terms = get_terms( 'product_cat', array(
        'hide_empty' => 0,
        'parent' => 0
  	));

  	// loop thru the records
	foreach( $terms as $term){
		
		// get the thumbnail id
		$thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
								
		// get the thumbnail image
		$image_url = wp_get_attachment_url( $thumbnail_id, 'full_size' );

		// create the image file
		$image = "<img src='".$image_url."' alt='".$name."' />";

		// get the url
		$url = get_term_link($term->term_id);
		// html
		$html = '
		<div class="view view-sixth">
			<div class="image-container" style="background-image:url('.$image_url.');"></div>  
		    <h2>'.$term->name.'</h2>  
		    <div class="mask"> 
			    <p>'.$term->description.'</p>  
			    <a href="'.$url.'" class="info"></a>  
			</div>  
		</div>
		';
		

		// store the response
		$response = $response . $html;
	}

	return $response;
}
add_shortcode( 'parent-categories', 'get_parent_categories');





