<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php start_short(); ?>
	
			<?php 
				// get all attached resources
				$args = array( 
							'post_type' => 'product',							
						);
				$the_query = new WP_Query( $args );

				// The Loop
				if ( $the_query->have_posts() ) {
					
					while ( $the_query->have_posts() ) {
						// set the post
						$the_query->the_post();

						// get the id
						echo get_the_id();

						// get the title
						echo get_the_title() . '<br />';
						
						// get the product meta data
						$product = get_post_meta( get_the_id() );
						//var_dump( $product );

						// get the attachment id
						$specification_id = $product['specification_file'][0];
						//echo $specification_id;

						// check if their is a file attached
						if($specification_id){
							// get the guid which is the file location
							echo '<a href="'.get_the_guid($specification_id).'">Download</a>';
						}
						
						
						echo '<hr />';
					}
					
					/* Restore original Post Data */
					wp_reset_postdata();
				} else {
					// no posts found
				}

				
				
			?>
			
		<?php end_short(); ?>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>